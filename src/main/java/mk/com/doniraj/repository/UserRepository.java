package mk.com.doniraj.repository;

import mk.com.doniraj.model.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends CrudRepository<User, Integer> {

  User findByEmail(String email);

  List<User> findAllByActivatedIsFalse();

  List<User> findAll();

  List<User> findAllByRole(String role);
}

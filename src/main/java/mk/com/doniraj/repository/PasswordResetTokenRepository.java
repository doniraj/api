package mk.com.doniraj.repository;

import mk.com.doniraj.model.PasswordResetToken;
import mk.com.doniraj.model.User;
import org.springframework.data.repository.CrudRepository;

public interface PasswordResetTokenRepository extends CrudRepository<PasswordResetToken, Integer> {

    PasswordResetToken findByToken(String token);

    PasswordResetToken findByUser(User user);
}

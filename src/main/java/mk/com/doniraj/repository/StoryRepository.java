package mk.com.doniraj.repository;

import mk.com.doniraj.model.Story;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StoryRepository extends CrudRepository<Story, Integer> {

    List<Story> findAllByOrderByDateCreatedAsc();

    List<Story> findAllByOrderByDateCreatedDesc();

    List<Story> findAllByPendingIsFalseOrderByDateCreatedDesc();

    List<Story> findAllByPendingIsFalseOrderByDateCreatedAsc();

    List<Story> findAllByPendingIsFalseOrderByFundsDesc();

    List<Story> findAllByPendingIsFalseOrderByFundsAsc();

    List<Story> findAllByOrderByFundsAsc();

    List<Story> findAllByOrderByFundsDesc();

    List<Story> findAllByUserId(int user_id);

    Integer countStoriesByUserId(int user_id);

    List<Story> findAllByPendingTrueOrderByDateCreatedDesc();

    List<Story> findAllByPendingTrueOrderByDateCreatedAsc();

    List<Story> findAllByPendingFalse();

    List<Story> findAll();

    List<Story> findAllByUserEmail(String email);

    Story findByTitle(String title);
}

package mk.com.doniraj.repository;

import mk.com.doniraj.model.BankAccount;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface BankAccountRepository extends CrudRepository<BankAccount, Integer> {
    List<BankAccount> getAllByUserId(Integer id);

    List<BankAccount> getAllByUserEmail(String email);
}

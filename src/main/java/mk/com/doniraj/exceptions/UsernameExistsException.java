package mk.com.doniraj.exceptions;

public class UsernameExistsException extends Exception {
    public UsernameExistsException(String email) {
        super("User with email " + email + " already exists in database");
    }
}

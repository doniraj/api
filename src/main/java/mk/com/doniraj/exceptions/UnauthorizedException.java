package mk.com.doniraj.exceptions;

public class UnauthorizedException extends Exception {

    public UnauthorizedException(String s) {
        super(s);
    }
}

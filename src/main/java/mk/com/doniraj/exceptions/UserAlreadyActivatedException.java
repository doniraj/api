package mk.com.doniraj.exceptions;

import mk.com.doniraj.model.User;

public class UserAlreadyActivatedException extends Exception {
    public UserAlreadyActivatedException(User user) {
        super("User " + user.getEmail() + " is already activated");
    }
}

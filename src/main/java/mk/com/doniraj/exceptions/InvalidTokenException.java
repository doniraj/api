package mk.com.doniraj.exceptions;

import mk.com.doniraj.model.VerificationToken;

public class InvalidTokenException extends Exception {

    public InvalidTokenException(VerificationToken token) {
        super("Token " + token.getToken() + " is not valid");
    }

    public InvalidTokenException(String confirmationKey) {
        super("Token " + confirmationKey + " is not valid");
    }
}

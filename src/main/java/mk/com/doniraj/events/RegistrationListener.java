package mk.com.doniraj.events;

import mk.com.doniraj.exceptions.UsernameExistsException;
import mk.com.doniraj.model.User;
import mk.com.doniraj.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

@Component
public class RegistrationListener implements ApplicationListener<OnRegistrationCompleteEvent> {


    private final UserService userService;

    @Autowired
    public RegistrationListener(UserService userService) {
        this.userService = userService;
    }

    public void onApplicationEvent(OnRegistrationCompleteEvent event) {
        try {
            this.confirmRegistration(event);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void confirmRegistration(OnRegistrationCompleteEvent event) throws UsernameExistsException {
        User user = event.getUser();
        userService.addUser(user);
    }
}

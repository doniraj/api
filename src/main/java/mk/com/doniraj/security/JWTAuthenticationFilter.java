package mk.com.doniraj.security;

import com.fasterxml.jackson.databind.ObjectMapper;
import mk.com.doniraj.model.JWT;
import mk.com.doniraj.model.UserDto;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

import static mk.com.doniraj.security.SecurityUtils.generateToken;


public class JWTAuthenticationFilter extends UsernamePasswordAuthenticationFilter {
    private AuthenticationManager authenticationManager;

    public JWTAuthenticationFilter(AuthenticationManager authenticationManager) {
        this.authenticationManager = authenticationManager;
        setFilterProcessesUrl("/api/users/login");
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request,
                                                HttpServletResponse response) throws BadCredentialsException {

        try {
            UserDto userDto = new ObjectMapper()
                    .readValue(request.getInputStream(), UserDto.class);

            String email = userDto.getEmail();
            String password = userDto.getPassword();

            mk.com.doniraj.model.User user = new mk.com.doniraj.model.User();
            user.setEmail(email);
            user.setUserPassword(password);

            ArrayList<GrantedAuthority> authorities = new ArrayList<>();
            SimpleGrantedAuthority authority = new SimpleGrantedAuthority(user.getRole());
            authorities.add(authority);

            return authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(
                            user.getEmail(),
                            user.getUserPassword(),
                            authorities
                    )
            );
        } catch (IOException e) {
            throw new BadCredentialsException("Parsing failed");
        }
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest request,
                                            HttpServletResponse response,
                                            FilterChain chain,
                                            Authentication authentication) throws IOException, ServletException {

        String username = ((User) authentication.getPrincipal()).getUsername();
        Collection<GrantedAuthority> role = ((User) authentication.getPrincipal()).getAuthorities();

        JWT jwt = new JWT(generateToken(username, role));
        ObjectMapper mapper = new ObjectMapper();
        response.getWriter().print(mapper.writeValueAsString(jwt));
        response.setContentType("application/json");
    }
}

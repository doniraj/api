package mk.com.doniraj.security;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import mk.com.doniraj.model.User;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.stream.Collectors;

public class SecurityUtils {
  static final String SECRET = System.getenv("JWT_SECRET_KEY");
  static final String TOKEN_PREFIX = "Bearer ";
  static final String HEADER_STRING = "Authorization";
  static final String SIGN_UP_URL = "/api/users/register";
  static final String ACTIVATE_URL = "/api/users/activate/**";
  static final String API_USERS_LOGIN = "/api/users/login";
  static final String API_USERS_REQUEST_RESET_PASSWORD = "/api/users/request-reset-password";
  static final String API_USERS_RESET_PASSWORD = "/api/users/reset-password";
  static final String API_STORIES = "/api/stories";
  static final String API_STORIES_WILDCARD = "/api/stories/*";
  static final String API_STORIES_ACTIVE = "/api/stories/active";
  static final String API_STORIES_PENDING = "/api/stories/pending*";
  static final String API_STORIES_APPROVE = "/api/stories/approve*";
  static final String API_STORIES_REJECT = "/api/stories/reject*";
  static final String API_STORIES_NEW = "/api/stories/new";
  static final String CONTACT = "/contact";
  private static final long EXPIRATION_TIME = 864_000_000; // 10 days

  public static String generateToken(String username, Collection<? extends GrantedAuthority> authorities) {
    Claims claims = Jwts.claims().setSubject(username);
    claims.put("scopes", authorities.stream().map(Object::toString).collect(Collectors.toList()));

    return Jwts.builder()
            .setClaims(claims)
            .setIssuedAt(new Date(System.currentTimeMillis()))
            .setExpiration(new Date(System.currentTimeMillis() + EXPIRATION_TIME))
            .signWith(SignatureAlgorithm.HS512, SECRET.getBytes())
            .compact();
  }

  public static String generateToken(User user) {
    ArrayList<GrantedAuthority> authorities = new ArrayList<>();
    SimpleGrantedAuthority authority = new SimpleGrantedAuthority(user.getRole());
    authorities.add(authority);

    return generateToken(user.getEmail(), authorities);
  }

  public static String generateToken(UserDetails user) {
    return generateToken(user.getUsername(), user.getAuthorities());
  }
}

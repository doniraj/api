package mk.com.doniraj.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "story", schema = "doniraj")
public class Story implements Serializable {

  @Id
  @GeneratedValue
  @Column(name = "id")
  private int id;

  @ManyToOne
  @JoinColumn(nullable = false, name = "user_id")
  private User user;

  @Column(name = "title")
  private String title;
  @Column(name = "date_created")
  private Date dateCreated;
  @Column(name = "funds")
  private Integer funds;
  @Column(name = "story")
  private String story;
  @Column(name = "pending")
  private boolean pending;
  @Column(name = "image_url")
  private String imageUrl;
  @Column(name = "count")
  private Short count = 0;
  @Temporal(TemporalType.DATE)
  @Column(name = "deadline")
  private Date deadline;

  public Story() {

  }

  public Story(User user,
               String title,
               Date dateCreated,
               Integer funds,
               String story,
               boolean pending,
               String imageUrl, Short count) {
    this.user = user;
    this.title = title;
    this.dateCreated = dateCreated;
    this.funds = funds;
    this.story = story;
    this.pending = pending;
    this.imageUrl = imageUrl;
    this.count = count;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public User getUser() {
    return user;
  }

  public void setUser(User user) {
    this.user = user;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public Date getDateCreated() {
    return dateCreated;
  }

  public void setDateCreated(Date dateCreated) {
    this.dateCreated = dateCreated;
  }

  public Integer getFunds() {
    return funds;
  }

  public void setFunds(Integer funds) {
    this.funds = funds;
  }

  public String getStory() {
    return story;
  }

  public void setStory(String story) {
    this.story = story;
  }

  public boolean isPending() {
    return pending;
  }

  public void setPending(boolean pending) {
    this.pending = pending;
  }

  public String getImageUrl() {
    return imageUrl;
  }

  public void setImageUrl(String imageUrl) {
    this.imageUrl = imageUrl;
  }

  public Short getCount() {
    return count;
  }

  public void setCount(Short count) {
    this.count = count;
  }

  @Override
  public String toString() {
    return "Story{" +
            "id=" + id +
            ", user=" + user +
            ", title='" + title + '\'' +
            ", dateCreated=" + dateCreated +
            ", funds=" + funds +
            ", story='" + story + '\'' +
            ", pending=" + pending +
            ", imageUrl='" + imageUrl + '\'' +
            ", count=" + count +
            '}';
  }

  public Date getDeadline() {
    return deadline;
  }

  public void setDeadline(Date deadline) {
    this.deadline = deadline;
  }
}

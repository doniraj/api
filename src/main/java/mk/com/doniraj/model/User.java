package mk.com.doniraj.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Set;
import java.util.logging.Logger;

@Entity
@Table(name = "users", schema = "doniraj")
public class User implements Serializable {
  @Transient
  private Logger logger = Logger.getLogger(this.getClass().getName());

  @Column(name = "id")
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private int id;
  @Column(name = "email")
  private String email;
  @Column(name = "user_password")
  private String userPassword;
  @Column(name = "acc_name")
  private String accName;
  @Column(name = "acc_surname")
  private String accSurname;
  @Temporal(TemporalType.DATE)
  @Column(name = "birthday")
  private Date birthday;
  @Column(name = "role")
  private String role = "ROLE_USER";
  @Column(name = "phone_number")
  private String phoneNumber;
  @Column(name = "avatar_url")
  private String avatarUrl;
  @Column(name = "activated")
  private boolean activated;
  @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true)
  @JsonManagedReference
  private Set<BankAccount> bankAccountSet;

  public User() {

    this.activated = false;
  }

  public User(String email) {
    this.email = email;
  }

  public User(String email,
              String userPassword,
              String accName,
              String accSurname,
              Date birthday,
              String role,
              String phoneNumber,
              String avatarUrl) {

    this.email = email;
    this.userPassword = userPassword;
    this.accName = accName;
    this.accSurname = accSurname;
    this.birthday = birthday;
    this.role = role;
    this.phoneNumber = phoneNumber;
    this.avatarUrl = avatarUrl;
    this.activated = false;
  }

  public boolean isActivated() {
    return activated;
  }

  public void setActivated(boolean activated) {
    this.activated = activated;
  }

  @Override
  public String toString() {
    ObjectMapper mapper = new ObjectMapper();
    try {
      return mapper.writeValueAsString(this);
    } catch (JsonProcessingException e) {
//            System.out.println(e.getMessage());
      logger.severe(e.getMessage());
      return null;
    }
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  @JsonIgnore
  public String getUserPassword() {
    return userPassword;
  }

  @JsonProperty
  public void setUserPassword(String userPassword) {
    this.userPassword = userPassword;
  }

  public String getAccName() {
    return accName;
  }

  public void setAccName(String accName) {
    this.accName = accName;
  }

  public String getAccSurname() {
    return accSurname;
  }

  public void setAccSurname(String accSurname) {
    this.accSurname = accSurname;
  }

  public Date getBirthday() {
    return birthday;
  }

  public void setBirthday(Date birthday) {
    this.birthday = birthday;
  }

  public String getRole() {
    return role;
  }

  public void setRole(String role) {
    this.role = role;
  }

  public String getPhoneNumber() {
    return phoneNumber;
  }

  public void setPhoneNumber(String phoneNumber) {
    this.phoneNumber = phoneNumber;
  }

  public String getAvatarUrl() {
    return avatarUrl;
  }

  public void setAvatarUrl(String avatarUrl) {
    this.avatarUrl = avatarUrl;
  }

  public Set<BankAccount> getBankAccountSet() {
    return bankAccountSet;
  }

  public void setBankAccountSet(Set<BankAccount> bankAccountSet) {
    this.bankAccountSet = bankAccountSet;
  }
}

package mk.com.doniraj.model;

public class ContactMessage {
  public String name;
  public String email;
  public String phone;
  public String message;

  public ContactMessage(String name, String email, String phone, String message) {
    this.name = name;
    this.email = email;
    this.phone = phone;
    this.message = message;
  }

  public ContactMessage() {

  }
}

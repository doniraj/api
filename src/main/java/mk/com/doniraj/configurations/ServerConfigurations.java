package mk.com.doniraj.configurations;

import org.apache.catalina.connector.Connector;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.EmbeddedWebApplicationContext;
import org.springframework.boot.context.embedded.tomcat.TomcatEmbeddedServletContainer;
import org.springframework.context.annotation.Configuration;

import java.net.InetAddress;
import java.net.UnknownHostException;

@Configuration
public class ServerConfigurations {

    private final EmbeddedWebApplicationContext appContext;

    @Autowired
    public ServerConfigurations(EmbeddedWebApplicationContext appContext) {
        this.appContext = appContext;
    }

    public String getBaseUrl() throws UnknownHostException {
        Connector connector = ((TomcatEmbeddedServletContainer) appContext.getEmbeddedServletContainer()).getTomcat().getConnector();
        String scheme = connector.getScheme();
        String ip = InetAddress.getLocalHost().getHostAddress();
        int port = connector.getPort();
        String contextPath = appContext.getServletContext().getContextPath();
        return scheme + "://" + ip + ":" + port + contextPath;
    }

}

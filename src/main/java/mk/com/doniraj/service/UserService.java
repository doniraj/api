package mk.com.doniraj.service;

import com.auth0.jwt.exceptions.TokenExpiredException;
import mk.com.doniraj.exceptions.InvalidTokenException;
import mk.com.doniraj.exceptions.UnauthorizedException;
import mk.com.doniraj.exceptions.UserAlreadyActivatedException;
import mk.com.doniraj.exceptions.UsernameExistsException;
import mk.com.doniraj.model.BankAccount;
import mk.com.doniraj.model.PasswordResetToken;
import mk.com.doniraj.model.User;
import mk.com.doniraj.model.VerificationToken;
import mk.com.doniraj.repository.BankAccountRepository;
import mk.com.doniraj.repository.PasswordResetTokenRepository;
import mk.com.doniraj.repository.UserRepository;
import mk.com.doniraj.repository.VerificationTokenRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.DataRetrievalFailureException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.UUID;

@Service
public class UserService implements UserDetailsService {

  private final UserRepository userRepository;
  private final VerificationTokenRepository verificationTokenRepository;
  private final PasswordResetTokenRepository resetTokenRepository;
  private final BankAccountRepository bankAccountRepository;
  private final PasswordEncoder passwordEncoder;

  /**
   * Constructor.
   *
   * @param userRepository              autowired
   * @param verificationTokenRepository autowired
   * @param resetTokenRepository        autowired
   * @param bankAccountRepository       autowired
   * @param passwordEncoder             autowired
   */
  @Autowired
  public UserService(UserRepository userRepository,
                     VerificationTokenRepository verificationTokenRepository,
                     PasswordResetTokenRepository resetTokenRepository,
                     BankAccountRepository bankAccountRepository, PasswordEncoder passwordEncoder) {
    this.userRepository = userRepository;
    this.verificationTokenRepository = verificationTokenRepository;
    this.resetTokenRepository = resetTokenRepository;
    this.bankAccountRepository = bankAccountRepository;
    this.passwordEncoder = passwordEncoder;
  }

  public ArrayList<User> findByRole(String role) {
    return (ArrayList<User>) userRepository.findAllByRole(role);
  }

  /**
   * Save user to database.
   *
   * @param user the user to be saved in database
   * @return true if save is successful
   * @throws DataIntegrityViolationException save failed
   */
  public boolean saveUser(User user) throws DataIntegrityViolationException {
    if (userRepository.save(user) == null) {
      throw new DataIntegrityViolationException(
              "Operation save user cannot be performed. Data Integrity violated"
      );
    } else {
      return true;
    }
  }

  /**
   * Add new user to database.
   *
   * @param user new user to add
   * @return user from database
   * @throws DataIntegrityViolationException save to db failed
   * @throws UsernameExistsException         email already exists in db
   */
  public User addUser(User user) throws DataIntegrityViolationException, UsernameExistsException {

    if (userRepository.findByEmail(user.getEmail()) != null) {
      throw new UsernameExistsException(user.getEmail());
    }

    user.setUserPassword(passwordEncoder.encode(user.getUserPassword()));

    if (userRepository.save(user) == null) {
      throw new DataIntegrityViolationException(
              "Operation save user cannot be performed. Data Integrity violated"
      );
    }

    return userRepository.findByEmail(user.getEmail());
  }

  /**
   * Generate user verification token and save to database.
   *
   * @param user for whom the token is generated
   * @return verification token
   */
  public VerificationToken generateVerificationToken(User user) {

    String token = UUID.randomUUID().toString();

    VerificationToken verificationToken = new VerificationToken(token, user);
    if (verificationTokenRepository.save(verificationToken) == null) {
      throw new DataIntegrityViolationException(
              "Operation save verification token cannot be performed. Data Integrity violated"
      );
    }

    return verificationToken;
  }


  /**
   * Activate user from verification token.
   *
   * @param confirmationKey the key used to activate a user
   * @return {@link User} the now activated user
   * @throws UserAlreadyActivatedException user is already activated
   * @throws InvalidTokenException         provided token is not valid or retrieval from database has failed
   */
  public User activateUser(String confirmationKey)
          throws UserAlreadyActivatedException, InvalidTokenException {

    VerificationToken token = verificationTokenRepository.findByToken(confirmationKey);
    if (token == null) {
      throw new InvalidTokenException(confirmationKey);
    }

    User user = token.getUser();

    Calendar calendar = Calendar.getInstance();

    if (token.getExpiryDate().getTime() - calendar.getTime().getTime() < 0) {
      throw new TokenExpiredException("The activation token has expired");
    }

    if (user != null) {
      if (user.isActivated()) {
        throw new UserAlreadyActivatedException(user);
      }

      user.setActivated(true);

      if (userRepository.save(user) == null) {
        throw new DataIntegrityViolationException("Operation save user cannot be performed. Data Integrity violated");
      }

      return user;
    }
    throw new InvalidTokenException(token);
  }

  /**
   * Get list of all users.
   *
   * @return List of all users in database
   * @throws DataRetrievalFailureException failed to retrieve users from database
   */
  public List<User> getAllUsers() throws DataRetrievalFailureException {
    List<User> users = userRepository.findAll();
    if (users == null) {
      throw new DataRetrievalFailureException("Users cannot be retrieved");
    }
    return users;
  }

  /**
   * Delete single user from database.
   *
   * @param id user to be deleted
   * @return true if delete is successful
   * @throws DataIntegrityViolationException failed to delete user
   */
  public boolean deleteUser(Integer id) throws DataIntegrityViolationException {
    userRepository.delete(id);
    if (userRepository.findOne(id) != null)
      throw new DataIntegrityViolationException("Operation delete user not performed");
    return true;
  }

  /**
   * Get single user from database.
   *
   * @param id of the user to be retrieved
   * @return {@link User}
   * @throws DataRetrievalFailureException user not found
   */
  public User getUser(Integer id) throws DataRetrievalFailureException {
    User user = userRepository.findOne(id);
    if (user == null) {
      throw new DataRetrievalFailureException("User cannot be retrieved");
    }
    return user;
  }


  /**
   * Request password reset token.
   *
   * @param email user's email for which a token is to be generated
   * @return {@link PasswordResetToken}
   * @throws DataRetrievalFailureException   failed to find user
   * @throws DataIntegrityViolationException failed to write token to database
   */
  public PasswordResetToken requestResetPassword(String email)
          throws DataRetrievalFailureException,
          DataIntegrityViolationException {

    User user = userRepository.findByEmail(email);

    if (user == null) {
      throw new DataRetrievalFailureException("User cannot be retrieved");
    }

    String token = UUID.randomUUID().toString();
    PasswordResetToken resetToken = new PasswordResetToken(token, user);

    if (resetTokenRepository.save(resetToken) == null) {
      throw new DataIntegrityViolationException("Operation save verification token cannot be performed. Data Integrity violated");
    }

    return resetToken;
  }

  /**
   * Reset password using reset token.
   *
   * @param resetToken  token used to reset password
   * @param newPassword new password to write to user
   * @throws DataRetrievalFailureException   invalid token
   * @throws DataIntegrityViolationException failed to new password to database
   */
  public void resetPassword(String resetToken, String newPassword)
          throws DataRetrievalFailureException,
          DataIntegrityViolationException {

    PasswordResetToken token = resetTokenRepository.findByToken(resetToken);

    if (token == null) {
      throw new DataRetrievalFailureException("Token cannot be retrieved");
    }

    User user = token.getUser();


    user.setUserPassword(passwordEncoder.encode(newPassword));

    if (userRepository.save(user) == null) {
      throw new DataIntegrityViolationException("Operation save user cannot be performed. Data Integrity violated");
    }
  }

  @Override
  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
    User foundUser = userRepository.findByEmail(username);
    if (foundUser == null) {
      throw new UsernameNotFoundException(username);
    }

    ArrayList<GrantedAuthority> authorities = new ArrayList<>();
    SimpleGrantedAuthority authority = new SimpleGrantedAuthority(foundUser.getRole());
    authorities.add(authority);

    return new org.springframework.security.core.userdetails.User(
            foundUser.getEmail(), foundUser.getUserPassword(), authorities
    );
  }

  /**
   * Update user details. DO NOT use for updating password, uses the old password only.
   *
   * @param user  new details
   * @param email old user's email
   */
  public void update(User user, String email) {

    User oldUser = userRepository.findByEmail(email);
    user.setId(oldUser.getId());
    user.setBankAccountSet(oldUser.getBankAccountSet());
    user.setUserPassword(oldUser.getUserPassword());
    saveUser(user);
  }


  /**
   * Delete bank account.
   *
   * @param accountId account to be deleted.
   * @param email     of user trying to delete, null if admin is deleting.
   * @throws UnauthorizedException email doesn't match bank account's email
   */
  public void deleteAccount(int accountId, String email) throws UnauthorizedException {
    if (email == null) {
      bankAccountRepository.delete(accountId);
      return;
    }

    if (bankAccountRepository.findOne(accountId).getUser().getEmail().equalsIgnoreCase(email)) {
      bankAccountRepository.delete(accountId);
    } else {
      throw new UnauthorizedException("Bank account doesn't belong to this user");
    }

  }

  /**
   * Add new bank account.
   *
   * @param account bank account to be added.
   * @param email   of user adding bank account.
   */
  public void addBankAccount(BankAccount account, String email) {
    User user = userRepository.findByEmail(email);
    account.setUser(user);
    bankAccountRepository.save(account);
  }

  /**
   * Get user by email.
   *
   * @param email of user to be found.
   * @return user.
   */
  public User getUserByEmail(String email) {
    return userRepository.findByEmail(email);
  }
}



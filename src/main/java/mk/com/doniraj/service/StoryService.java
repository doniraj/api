package mk.com.doniraj.service;

import mk.com.doniraj.model.Story;
import mk.com.doniraj.model.User;
import mk.com.doniraj.repository.StoryRepository;
import mk.com.doniraj.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.DataRetrievalFailureException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

@Service
public class StoryService {

    private final UserRepository userRepository;
    private final StoryRepository storyRepository;
    private Logger logger = Logger.getLogger(StoryService.class.getName());

    @Autowired
    public StoryService(UserRepository userRepository, StoryRepository storyRepository) {
        this.userRepository = userRepository;
        this.storyRepository = storyRepository;
    }

    /**
     * Get all stories.
     *
     * @return {@link List} of all stories
     * @throws DataRetrievalFailureException failed to retrieve stories
     */
    public List<Story> getAllStories() throws DataRetrievalFailureException {
        List<Story> stories = storyRepository.findAll();
        if (stories == null) throw new DataRetrievalFailureException("Stories cannot be retreived");
        return stories;
    }

    /**
     * Get individual story from id.
     *
     * @param id id of the story to be retrieved
     * @return {@link Story}
     * @throws DataRetrievalFailureException story not found or database error
     */
    public Story getStoryById(int id) throws DataRetrievalFailureException {
        Story story = storyRepository.findOne(id);
        if (story == null) throw new DataRetrievalFailureException("Story cannot be retrieved");
        return story;
    }

    /**
     * Get individual story from title.
     *
     * @param title of the story to be retrieved
     * @return {@link Story}
     * @throws DataRetrievalFailureException story not found or database error
     */
    public Story getStoryByTitle(String title) throws DataRetrievalFailureException {
        Story story = storyRepository.findByTitle(title);
        if (story == null) throw new DataRetrievalFailureException("Story cannot be retrieved");
        return story;
    }

    /**
     * Get stories ordered by date ascending.
     *
     * @return {@link List} of stories
     * @throws DataRetrievalFailureException failed to retrieve stories
     */
    public List<Story> getStoriesOrderedByDateAsc() throws DataRetrievalFailureException {
        List<Story> stories = storyRepository.findAllByOrderByDateCreatedAsc();
        if (stories == null) throw new DataRetrievalFailureException("Stories cannot be retreived");
        return stories;
    }

    /**
     * Get stories ordered by date descending.
     *
     * @return {@link List} of stories
     * @throws DataRetrievalFailureException failed to retrieve stories
     */
    public List<Story> getStoriesOrderedByDateDesc() throws DataRetrievalFailureException {
        List<Story> stories = storyRepository.findAllByOrderByDateCreatedDesc();
        if (stories == null) throw new DataRetrievalFailureException("Stories cannot be retreived");
        return stories;
    }

    /**
     * Get stories ordered by funds ascending.
     *
     * @return {@link List} of stories
     * @throws DataRetrievalFailureException failed to retrieve stories
     */
    public List<Story> getStoriesOrderedByFundsAsc() throws DataRetrievalFailureException {
        List<Story> stories = storyRepository.findAllByOrderByFundsAsc();
        if (stories == null) throw new DataRetrievalFailureException("Stories cannot be retreived");
        return stories;
    }

    /**
     * Get stories ordered by funds descending.
     *
     * @return {@link List} of stories
     * @throws DataRetrievalFailureException failed to retrieve stories
     */
    public List<Story> getStoriesOrderedByFundsDesc() throws DataRetrievalFailureException {
        List<Story> stories = storyRepository.findAllByOrderByFundsDesc();
        if (stories == null) throw new DataRetrievalFailureException("Stories cannot be retreived");
        return stories;
    }

    /**
     * Get active stories ordered by date ascending.
     *
     * @return {@link List} of stories
     * @throws DataRetrievalFailureException failed to retrieve stories
     */
    public List<Story> getActiveStoriesOrderedByDateAsc() throws DataRetrievalFailureException {
        return storyRepository.findAllByPendingIsFalseOrderByDateCreatedAsc();
    }

    /**
     * Get active stories ordered by date descending.
     *
     * @return {@link List} of stories
     * @throws DataRetrievalFailureException failed to retrieve stories
     */
    public List<Story> getActiveStoriesOrderedByDateDesc() throws DataRetrievalFailureException {
        return storyRepository.findAllByPendingIsFalseOrderByDateCreatedDesc();
    }

    /**
     * Get active stories ordered by funds ascending.
     *
     * @return {@link List} of stories
     * @throws DataRetrievalFailureException failed to retrieve stories
     */
    public List<Story> getActiveStoriesOrderedByFundsAsc() throws DataRetrievalFailureException {
        return storyRepository.findAllByPendingIsFalseOrderByFundsAsc();
    }

    /**
     * Get active stories ordered by funds descending.
     *
     * @return {@link List} of stories
     * @throws DataRetrievalFailureException failed to retrieve stories
     */
    public List<Story> getActiveStoriesOrderedByFundsDesc() throws DataRetrievalFailureException {
        return storyRepository.findAllByPendingIsFalseOrderByFundsDesc();
    }

    /**
     * Get stories ordered by user Id.
     *
     * @return {@link List} of stories
     * @throws DataRetrievalFailureException failed to retrieve stories
     */
    public List<Story> getStoriesByUserId(Integer userId) throws DataRetrievalFailureException {
        List<Story> stories = storyRepository.findAllByUserId(userId);
        if (stories == null) throw new DataRetrievalFailureException("Stories cannot be retreived");
        return stories;
    }

    /**
     * Count stories of user Id.
     *
     * @return {@link List} of stories
     * @throws DataRetrievalFailureException failed to retrieve stories
     */
    public Integer countStoriesByUserId(Integer user) throws DataRetrievalFailureException {
        Integer count = storyRepository.countStoriesByUserId(user);
        if (count == null) throw new DataRetrievalFailureException("Stories count cannot be retreived");
        return count;
    }

    /**
     * Get all pending stories.
     *
     * @return {@link List} of stories
     * @throws DataRetrievalFailureException failed to retrieve stories
     */
    public List<Story> getPendingStories(String orderBy) throws DataRetrievalFailureException {
        List<Story> stories;
        if (orderBy != null && orderBy.equals("asc")) {
            stories = storyRepository.findAllByPendingTrueOrderByDateCreatedAsc();
        } else {
            stories = storyRepository.findAllByPendingTrueOrderByDateCreatedDesc();
        }
        if (stories == null) throw new DataRetrievalFailureException("Stories cannot be retreived");
        return stories;
    }

    /**
     * Get all approved stories.
     *
     * @return {@link List} of stories
     * @throws DataRetrievalFailureException failed to retrieve stories
     */
    public List<Story> getApprovedStories() throws DataRetrievalFailureException {
        List<Story> stories = storyRepository.findAllByPendingFalse();
        if (stories == null) throw new DataRetrievalFailureException("Stories cannot be retreived");
        return stories;
    }

    /**
     * Approve story from id.
     *
     * @param id of the story to be approved
     * @return true if approval is successful
     * @throws DataRetrievalFailureException   failed to find story from id
     * @throws DataIntegrityViolationException failed to approve story
     */
    public boolean approveStory(Integer id) throws DataRetrievalFailureException, DataIntegrityViolationException {
        Story story = storyRepository.findOne(id);

        if (story == null) throw new DataRetrievalFailureException("Story cannot be retreived");
        story.setPending(false);

        if (storyRepository.save(story) == null)
            throw new DataIntegrityViolationException("Operation save story cannot be performed. Data Integrity violated");
        return true;
    }

    /**
     * Add story.
     *
     * @param story to be added
     * @return true if db operation is successful
     * @throws DataIntegrityViolationException failed to write story to db
     */
    private boolean addStory(Story story) throws DataIntegrityViolationException {
        logger.info(story.toString());

        story.setPending(true);
        if (storyRepository.save(story) == null)
            throw new DataIntegrityViolationException("Operation save story cannot be performed. Data Integrity violated");
        return true;
    }

    /**
     * Delete story.
     *
     * @param id              of the story to be deleted
     * @param email           of the user trying to delete story
     * @param adminPrivileges should bypass security check?
     * @return true if delete is successful
     * @throws DataRetrievalFailureException   failed to find story with given id
     * @throws DataIntegrityViolationException failed to delete story
     * @throws BadCredentialsException         user not allowed to delete the story
     */
    public boolean deleteStory(Integer id, String email, Boolean adminPrivileges)
            throws DataRetrievalFailureException, DataIntegrityViolationException, BadCredentialsException {

        Story story = storyRepository.findOne(id);

        if (story == null) {
            throw new DataRetrievalFailureException("Story cannot be retrieved");
        }

        if (adminPrivileges) {
            storyRepository.delete(id);
            if (storyRepository.findOne(id) != null) {
                throw new DataIntegrityViolationException("Operation delete story not performed");
            }
        } else if (story.getUser().getEmail().equalsIgnoreCase(email)) {
            storyRepository.delete(story);
        } else {
            throw new BadCredentialsException("User is not allowed to remove this story");
        }

        if (storyRepository.findOne(story.getId()) != null) {
            throw new DataIntegrityViolationException("Operation delete story not performed");
        }
        return true;
    }

    @Deprecated
    public boolean deleteStoryAdmin(Integer id) {
        Story story = storyRepository.findOne(id);
        storyRepository.delete(story);

        return storyRepository.findOne(story.getId()) == null;
    }

    public void save(Story story) {
        if (storyRepository.save(story) == null) throw new DataIntegrityViolationException("Failed to save story");
    }


    /**
     * Update story.
     *
     * @param story      new story
     * @param previousId id of story to be updated
     * @param email      email of user trying to do the edit
     * @param isAdmin    is the user editing an admin?
     * @return true if edit is successful
     * @throws DataRetrievalFailureException failed to find previous story with id
     * @throws BadCredentialsException       user not allowed to edit this story
     */
    public boolean updateStory(Story story, int previousId, String email, Boolean isAdmin)
            throws DataIntegrityViolationException, BadCredentialsException, DataRetrievalFailureException {

        Story oldStory = storyRepository.findOne(previousId);

        if (oldStory == null) {
            throw new DataRetrievalFailureException("Story cannot be retreived");
        }

        boolean allowed = story.getUser().getEmail().equalsIgnoreCase(email) &&
                oldStory.getUser().getEmail().equalsIgnoreCase(email) ||
                isAdmin;

        if (allowed) {
            story.setDateCreated(oldStory.getDateCreated());
            return this.addStory(story);
        } else {
            throw new BadCredentialsException("User is not allowed to update this story");
        }
    }

    @Deprecated
    public boolean updateStoryAdmin(Story story, int id) {
        story.setDateCreated(storyRepository.findOne(id).getDateCreated());
        return this.addStory(story);
    }


    /**
     * Add new story.
     *
     * @param story new story to be added.
     * @param email email of user adding story
     * @return true if insert is successful
     * @throws DataIntegrityViolationException failed to write story to database
     */
    public boolean addStory(Story story, String email) throws DataIntegrityViolationException {
        logger.info(story.toString());

        User user = userRepository.findByEmail(email);
        story.setDateCreated(new Date());
        story.setUser(user);

        return this.addStory(story);
    }

    /**
     * Delete story from admin.
     *
     * @param id story to be deleted
     * @return true if delete successful
     * @throws DataRetrievalFailureException   failed to retrieve story from db
     * @throws DataIntegrityViolationException failed to delete story from db
     * @deprecated
     */
    @Deprecated
    public boolean rejectStory(Integer id) throws DataRetrievalFailureException, DataIntegrityViolationException {
        storyRepository.delete(id);
        return storyRepository.findOne(id) == null;
    }

    /**
     * Find all stories from given user's email
     *
     * @param email to search for stories
     * @return list of stories
     */
    public List<Story> findAllByEmail(String email) {
        return storyRepository.findAllByUserEmail(email);
    }
}

package mk.com.doniraj.service;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import mk.com.doniraj.model.ContactMessage;
import mk.com.doniraj.model.PasswordResetToken;
import mk.com.doniraj.model.User;
import mk.com.doniraj.model.VerificationToken;
import net.sargue.mailgun.Configuration;
import net.sargue.mailgun.Mail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Map;

@Service
public class MailService {

  private static final String API_KEY = System.getenv("MAILGUN_API_KEY");
  private static final String MAILGUN_DOMAIN = System.getenv("MAILGUN_DOMAIN");
  private static final String HOST_NAME = "https://doniraj-frontend.herokuapp.com/";
  private Configuration configuration;
  private String from;
  private String to;
  private String subject;
  private String text;
  private String html; // TODO: 13.10.17 stylize later

  @Autowired
  public MailService() {
    from = "Doniraj.com.mk <noreply@doniraj.com.mk>"; // TODO: 13.10.17 save in application.properties
    configuration = new Configuration()
            .domain(MAILGUN_DOMAIN)
            .apiKey(API_KEY)
            .from("Doniraj Postman", "noreply@doniraj.mk");

  }


  /**
   * Convert object fields to Map ignoring null fields.
   *
   * @return Map
   */
  private Map toMap() {
    ObjectMapper mapper = new ObjectMapper();
    mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
    return mapper.convertValue(this, Map.class);
  }

  @Override
  public String toString() {
    ObjectMapper mapper = new ObjectMapper();
    try {
      return mapper.writeValueAsString(this);
    } catch (JsonProcessingException e) {
      System.out.println(e.getMessage());
      return null;
    }
  }

  /**
   * Add user details for activation message
   *
   * @param user              the user for whom the message is intended
   * @param verificationToken generated verification token
   */
  public void sendActivationMessage(User user, VerificationToken verificationToken) {

    text = "Здраво, " + user.getAccName() + "\n" +
            "За активација на Вашиот профил кликнете тука:\n" +
            HOST_NAME +
            "activate/" +
            verificationToken;

    to = user.getEmail();
    subject = "Активација на корисничка сметка";

    sendSimpleMessage();
  }

  public void sendPasswordResetMessage(PasswordResetToken resetToken) {

    User user = resetToken.getUser();
    // TODO: 24.10.17 duplicate code, fix this

    text = "Здраво, " + user.getAccName() + "\n" +
            "За ресетирање на Вашата лозинка кликнете тука:\n" +
            HOST_NAME +
            "/reset/" +
            resetToken;

    to = user.getEmail();
    subject = "Ресетирање на лозинка";
    sendSimpleMessage();
  }

  public void sendContactMessage(ContactMessage message, ArrayList<String> sendTo)
          throws RuntimeException {

    text = message.message;
    from = "contact@doniraj.mk";
    text += "\nPhone: " + message.phone;
    subject = "[" + message.email + "] Contact request: " + message.name;

    sendTo.forEach(recipient -> {
      to = recipient;
//      sendSimpleMessage();
      Mail.using(configuration)
              .to(to)
              .replyTo(message.email)
              .subject(subject)
              .text(text)
              .build()
              .send();
    });

  }

  /**
   * Send plain text email message.
   */
  private void sendSimpleMessage() {

//    String url = "https://api.mailgun.net/v3/" +
//            MAILGUN_DOMAIN +
//            "/messages";
//
//    Map map = this.toMap();
//
//    HttpResponse<JsonNode> request = Unirest.post(url)
//            .basicAuth("api", API_KEY)
//            .fields(map)
//            .asJson();
//
//    System.out.println(request.toString());

    Mail.using(configuration)
            .to(to)
            .subject(subject)
            .text(text)
            .build()
            .send();

  }

  public String getFrom() {
    return from;
  }

  public void setFrom(String from) {
    this.from = from;
  }

  public String getTo() {
    return to;
  }

  public String getSubject() {
    return subject;
  }

  public void setSubject(String subject) {
    this.subject = subject;
  }

  public String getText() {
    return text;
  }

  public void setText(String text) {
    this.text = text;
  }

  public String getHtml() {
    return html;
  }

  public void setHtml(String html) {
    this.html = html;
  }

  public void notifyUserChanged(User user) {

  }
}

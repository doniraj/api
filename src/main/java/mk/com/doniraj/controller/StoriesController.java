package mk.com.doniraj.controller;


import mk.com.doniraj.exceptions.UnauthorizedException;
import mk.com.doniraj.model.Story;
import mk.com.doniraj.service.StoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.DataRetrievalFailureException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Scanner;
import java.util.logging.Logger;

@RestController
@RequestMapping("/api/stories")
public class StoriesController {

  private final StoryService storyService;
  private Logger logger = Logger.getLogger(StoriesController.class.getName());

  @Autowired
  public StoriesController(StoryService storyService) {
    this.storyService = storyService;
  }

  @GetMapping("")
  public ResponseEntity<List<Story>> getAllStories(@RequestParam(value = "orderBy", required = false)
                                                           String orderBy) throws DataRetrievalFailureException {

    if (orderBy == null)
      return new ResponseEntity<>(storyService.getApprovedStories(), HttpStatus.OK);

    switch (orderBy) {
      case "dateAsc":
        return new ResponseEntity<>(storyService.getActiveStoriesOrderedByDateAsc(), HttpStatus.OK);
      case "dateDesc":
        return new ResponseEntity<>(storyService.getActiveStoriesOrderedByDateDesc(), HttpStatus.OK);
      case "fundsAsc":
        return new ResponseEntity<>(storyService.getActiveStoriesOrderedByFundsAsc(), HttpStatus.OK);
      case "fundsDesc":
        return new ResponseEntity<>(storyService.getActiveStoriesOrderedByFundsDesc(), HttpStatus.OK);
      default:
        return new ResponseEntity<>(storyService.getApprovedStories(), HttpStatus.OK);
    }
  }

  /**
   * Returns all stories from the current user
   *
   * @param authentication current user
   * @return List of stories
   */
  @GetMapping("my")
  public ResponseEntity<List<Story>> getMyStories(Authentication authentication) {
    String email = authentication.getName();
    return ResponseEntity.ok(storyService.findAllByEmail(email));
  }


  /**
   * Adds a story from a JSON format into the database
   *
   * @param story created by JSON
   * @return ResponseEntity with HttpStatus
   */
  @PostMapping("new")
  public ResponseEntity addStory(@RequestBody Story story, Authentication authentication) throws
          DataIntegrityViolationException, DataRetrievalFailureException {
    logger.info(story.toString());

    String email = authentication.getName();

    if (storyService.addStory(story, email)) {
      logger.info("New story: " + story.toString());
      return new ResponseEntity(HttpStatus.OK);
    } else {
      logger.severe("Failed to save story: " + story.toString());
      return new ResponseEntity(HttpStatus.BAD_REQUEST);
    }
  }

  @GetMapping(value = "/user/{userId}")
  public ResponseEntity<List<Story>> getAllStoriesByUserId(@PathVariable("userId") int userId) throws DataRetrievalFailureException {
    return new ResponseEntity<>(storyService.getStoriesByUserId(userId), HttpStatus.OK);
  }

  /**
   * Deletes a story from the database, given its id
   *
   * @param id taken as a path variable from the URL
   * @return ResponseEntity with HttpStatus
   */
  @DeleteMapping("/{id}")
  public ResponseEntity deleteStory(@PathVariable int id, Authentication authentication)
          throws DataRetrievalFailureException, DataIntegrityViolationException, UnauthorizedException {

    if (authentication == null) throw new UnauthorizedException("No authentication present");

    boolean admin = authentication.getAuthorities().contains(new SimpleGrantedAuthority("ROLE_ADMIN"));

    String email = authentication.getName();

    if (storyService.deleteStory(id, email, admin)) {
      logger.info("Deleted story by: " + email);
      return new ResponseEntity(HttpStatus.OK);
    }
    return new ResponseEntity(HttpStatus.BAD_REQUEST);
  }

  @GetMapping(value = "/{id_or_title}")
  public ResponseEntity<Story> getStoryById(@PathVariable(name = "id_or_title") String storyId) throws
          DataRetrievalFailureException {

    Scanner scanner = new Scanner(storyId);
    Story story;

    if (scanner.hasNextInt()) {
      story = storyService.getStoryById(Integer.parseInt(storyId));
    } else {
      story = storyService.getStoryByTitle(storyId);
    }

    // increase story view count
    Short count = story.getCount();
    if (count == null) count = 0;
    story.setCount(++count);

    storyService.save(story);

    return new ResponseEntity<>(story, HttpStatus.OK);
  }

  /**
   * Updates a story from the database, given it's id. Updates all non-required fields
   *
   * @param id    taken as a path variable from the URL
   * @param story the updated version from the story created by JSON
   * @return ResponseEntity with HttpStatus
   */
  @PostMapping("{id}")
  public ResponseEntity updateStory(@PathVariable int id,
                                    @RequestBody Story story,
                                    Authentication authentication)
          throws DataIntegrityViolationException, DataRetrievalFailureException {

    boolean admin = authentication.getAuthorities().contains(new SimpleGrantedAuthority("ROLE_ADMIN"));
    String email = authentication.getName();

    if (storyService.updateStory(story, id, email, admin)) {
      logger.info("Updated story by " + email + ": " + story.toString());
      return new ResponseEntity(HttpStatus.CREATED);
    } else throw new DataIntegrityViolationException("Delete failed");
  }


  /**
   * Get all pending stories.
   *
   * @return JsonArray of all pending stories.
   */
  @GetMapping(value = "/pending")
  @PreAuthorize("hasRole('ROLE_ADMIN')")
  public ResponseEntity<List<Story>> getAllPendingStories(@RequestParam(value = "orderBy", required = false)
                                                                  String order) throws DataRetrievalFailureException {
    return new ResponseEntity<>(storyService.getPendingStories(order), HttpStatus.OK);
  }

  /**
   * Approve story.
   *
   * @param storyId 27
   * @return Created
   */
  @PostMapping("/approve/{storyId}")
  @PreAuthorize("hasRole('ROLE_ADMIN')")
  public ResponseEntity postApproveStory(@PathVariable Integer storyId, Authentication authentication)
          throws DataRetrievalFailureException, DataIntegrityViolationException {
    if (storyService.approveStory(storyId)) {
      logger.info("Approved story " + storyId + " by " + authentication.getName());
      return new ResponseEntity(HttpStatus.CREATED);
    } else {
      throw new DataIntegrityViolationException("Failed to approve story");
    }
  }


  /**
   * Reject story.
   *
   * @param storyId 27
   * @return true
   */
  @PostMapping("/reject/{storyId}")
  @PreAuthorize("hasRole('ROLE_ADMIN')")
  public ResponseEntity postRejectStory(@PathVariable Integer storyId, Authentication authentication)
          throws DataRetrievalFailureException, DataIntegrityViolationException {
    if (storyService.deleteStory(storyId, null, true)) {
      logger.info("Rejected story " + storyId + " by " + authentication.getName());
      return new ResponseEntity(HttpStatus.OK);
    } else {
      return new ResponseEntity(HttpStatus.BAD_REQUEST);
    }
  }
}

package mk.com.doniraj.controller;

import mk.com.doniraj.configurations.ServerConfigurations;
import mk.com.doniraj.exceptions.UsernameExistsException;
import mk.com.doniraj.model.User;
import mk.com.doniraj.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import java.net.UnknownHostException;

@Controller
@RequestMapping("/api/tests")
public class TestRoutes {

    private final UserService userService;
    private final ServerConfigurations configurations;

    @Autowired
    public TestRoutes(UserService userService, ServerConfigurations configurations) {
        this.userService = userService;
        this.configurations = configurations;
    }

    @PostMapping("/add-user")
    public ResponseEntity<User> addUser(@RequestBody User user) throws UsernameExistsException {
        userService.addUser(user);
        return new ResponseEntity<>(user, HttpStatus.CREATED);
    }

    @GetMapping("/host")
    public String getHost() throws UnknownHostException {
        System.out.println(configurations.getBaseUrl());
        return configurations.getBaseUrl();
    }
}

package mk.com.doniraj.controller;


import mk.com.doniraj.exceptions.InvalidTokenException;
import mk.com.doniraj.exceptions.UnauthorizedException;
import mk.com.doniraj.exceptions.UserAlreadyActivatedException;
import mk.com.doniraj.exceptions.UsernameExistsException;
import mk.com.doniraj.model.*;
import mk.com.doniraj.service.MailService;
import mk.com.doniraj.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.DataRetrievalFailureException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Scanner;
import java.util.logging.Logger;

import static mk.com.doniraj.security.SecurityUtils.generateToken;

@RestController
@RequestMapping("/api/users")
public class UsersController {

  private final UserService userService;
  private final MailService mailService;
  private Logger logger = Logger.getLogger(this.getClass().getName());

  @Autowired
  public UsersController(UserService userService,
                         MailService mailService) {
    this.userService = userService;
    this.mailService = mailService;
  }


  @PostMapping("/register")
  public ResponseEntity addUser(@RequestBody User user, BindingResult result)
          throws UsernameExistsException {

    if (result.hasErrors()) {
      logger.warning("Bad request: " + user.toString() + result.toString());
      return new ResponseEntity(HttpStatus.BAD_REQUEST);
    }

//            eventPublisher.publishEvent(new OnRegistrationCompleteEvent(user));

    User newUser = userService.addUser(user);
    VerificationToken verificationToken = userService.generateVerificationToken(newUser);
    mailService.sendActivationMessage(user, verificationToken);

    logger.info("New user: " + newUser.toString());

    return new ResponseEntity(HttpStatus.CREATED);
  }


  @PostMapping(value = "/{id}")
  public ResponseEntity<User> updateUser(@RequestBody User user,
                                         Authentication authentication,
                                         @PathVariable Integer id)
          throws DataIntegrityViolationException {

    user.setId(id);

    if (authentication.getAuthorities().contains(new SimpleGrantedAuthority("ROLE_ADMIN"))) {
      userService.update(user, user.getEmail());
      mailService.notifyUserChanged(user);

      logger.info("Force updated user by " + authentication.getName() + ": " + user.toString());
    } else {
      String email = authentication.getName();
      userService.update(user, email);

      logger.info("Updated user: " + user.toString());
    }
    return new ResponseEntity<>(HttpStatus.OK);
  }

  @GetMapping(value = "")
  public ResponseEntity<List<User>> getAllUsers() throws DataRetrievalFailureException {
    return new ResponseEntity<>(userService.getAllUsers(), HttpStatus.OK);
  }

  @GetMapping(value = "/{id_or_email:.+}")
  public ResponseEntity<User> getUserById(@PathVariable(name = "id_or_email") String user)
          throws DataRetrievalFailureException {

    Scanner scanner = new Scanner(user);

    if (scanner.hasNextInt()) {
      return new ResponseEntity<>(userService.getUser(Integer.parseInt(user)), HttpStatus.OK);
    } else {
      return new ResponseEntity<>(userService.getUserByEmail(user), HttpStatus.OK);
    }
  }

  @DeleteMapping(value = "/{id}")
  public ResponseEntity deleteUser(@PathVariable Integer id)
          throws DataRetrievalFailureException, DataIntegrityViolationException {
    userService.deleteUser(id);
    logger.info("Deleted user: " + id);
    return new ResponseEntity(HttpStatus.OK);
  }


  @GetMapping("/activate/{token}")
  public ResponseEntity<JWT> confirmRegistration(@PathVariable String token)
          throws DataRetrievalFailureException,
          DataIntegrityViolationException,
          InvalidTokenException,
          UserAlreadyActivatedException {

    User user = userService.activateUser(token);

    if (user != null) {
      // TODO: 25.11.17 check if this is the best way
      logger.info("Activated user: " + user.toString());
      return new ResponseEntity<>(new JWT(generateToken(user)), HttpStatus.CREATED);
    }
    throw new RuntimeException("Activation failed");
  }


  @PostMapping("/request-reset-password")
  @PreAuthorize("permitAll()")
  public ResponseEntity requestPasswordReset(@RequestParam String resetEmail,
                                             BindingResult result)
          throws DataRetrievalFailureException,
          DataIntegrityViolationException {

    if (result.hasErrors()) {
      logger.warning("Bad request: " + resetEmail + result.toString());
      return new ResponseEntity(HttpStatus.BAD_REQUEST);
    }

    PasswordResetToken token = userService.requestResetPassword(resetEmail);
    mailService.sendPasswordResetMessage(token);

    logger.info("Reset token sent to: " + resetEmail);
    return new ResponseEntity<>(HttpStatus.CREATED);
  }


  @PostMapping("/reset-password")
  @PreAuthorize("permitAll()")
  public ResponseEntity resetPassword(@RequestBody PasswordResetDTO passwordReset)
          throws DataRetrievalFailureException, DataIntegrityViolationException {

    userService.resetPassword(passwordReset.getToken(), passwordReset.getPassword());

    logger.info("Password reset: " + passwordReset.getToken());
    return new ResponseEntity(HttpStatus.OK);
  }

  @DeleteMapping("bank-accounts/{bankId}")
  public ResponseEntity removeBankAccount(@PathVariable("bankId") String bankId, Authentication authentication)
          throws UnauthorizedException, NullPointerException {

    if (authentication.getName() == null) throw new NullPointerException("User has no email");

    if (authentication.getAuthorities().contains(new SimpleGrantedAuthority("ROLE_ADMIN"))) {
      userService.deleteAccount(Integer.parseInt(bankId), null);
      logger.info("Bank account force removed by " + authentication.getName() + ": " + bankId);
    } else {
      userService.deleteAccount(Integer.parseInt(bankId), authentication.getName());
    }

    return ResponseEntity.ok().build();
  }

  @PostMapping("bank-accounts")
  public ResponseEntity addBankAccount(@RequestBody BankAccount account,
                                       Authentication authentication) {

    userService.addBankAccount(account, authentication.getName());
    logger.info("Bank account added: " + account);
    return new ResponseEntity(HttpStatus.CREATED);
  }

}

package mk.com.doniraj.controller;

import com.mashape.unirest.http.exceptions.UnirestException;
import mk.com.doniraj.exceptions.UnauthorizedException;
import mk.com.doniraj.utilities.ApiError;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.DataRetrievalFailureException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.net.UnknownHostException;
import java.util.logging.Logger;

@ControllerAdvice
public class ExceptionHandlingController {
    private Logger logger = Logger.getGlobal();

    @ExceptionHandler(DataIntegrityViolationException.class)
    public ResponseEntity<ApiError> handleDataIntegrityViolationException(DataIntegrityViolationException e) {
        ApiError apiError = createApiError(HttpStatus.BAD_REQUEST, e.getMessage());
//        e.printStackTrace();
        logger.severe(e.getMessage());
        return new ResponseEntity<>(apiError, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(DataRetrievalFailureException.class)
    public ResponseEntity<ApiError> handleDataRetrievalFailureException(DataRetrievalFailureException e) {
        ApiError apiError = createApiError(HttpStatus.NOT_FOUND, e.getMessage());
//        e.printStackTrace();
        logger.severe(e.getMessage());
        return new ResponseEntity<>(apiError, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(UnirestException.class)
    public ResponseEntity<ApiError> handleUnirestException(UnirestException e) {
        ApiError apiError = createApiError(HttpStatus.BAD_REQUEST, e.getMessage());
//        e.printStackTrace();
        logger.severe(e.getMessage());
        return new ResponseEntity<>(apiError, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(UnknownHostException.class)
    public ResponseEntity<ApiError> handleUnkownHostException(UnknownHostException e) {
        ApiError apiError = createApiError(HttpStatus.BAD_REQUEST, e.getMessage());
//        e.printStackTrace();
        logger.severe(e.getMessage());
        return new ResponseEntity<>(apiError, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(UnauthorizedException.class)
    public ResponseEntity<ApiError> handleUnauthorizedException(UnauthorizedException e) {
        ApiError apiError = createApiError(HttpStatus.UNAUTHORIZED, e.getMessage());
//        e.printStackTrace();
        logger.severe(e.getMessage());
        return new ResponseEntity<>(apiError, HttpStatus.UNAUTHORIZED);
    }

    private ApiError createApiError(HttpStatus httpStatus, String message) {
        ApiError apiError = new ApiError();
        apiError.setStatus(httpStatus);
        apiError.setMessage(message);
        return apiError;
    }

}

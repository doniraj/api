package mk.com.doniraj.controller;

import mk.com.doniraj.model.ContactMessage;
import mk.com.doniraj.model.User;
import mk.com.doniraj.service.MailService;
import mk.com.doniraj.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.ArrayList;
import java.util.logging.Logger;
import java.util.stream.Collectors;

@Controller
public class BaseController {
  private final UserService userService;
  private final MailService mailService;
  private Logger logger = Logger.getLogger(this.getClass().getName());

  @Autowired
  public BaseController(UserService userService,
                        MailService mailService) {
    this.userService = userService;
    this.mailService = mailService;
  }

  @PostMapping("contact")
  public ResponseEntity contactAdmins(@RequestBody ContactMessage message)
          throws RuntimeException {
    ArrayList<User> admins = userService.findByRole("ROLE_ADMIN");
    ArrayList<String> emails = admins.stream().map(User::getEmail).collect(Collectors
            .toCollection(ArrayList::new));

    mailService.sendContactMessage(message, emails);

    return ResponseEntity.ok().build();
  }
}

CREATE TABLE password_reset_token (
  id          INT PRIMARY KEY AUTO_INCREMENT,
  token       VARCHAR(255),
  user_id     INT NOT NULL,
  expiry_date DATETIME,
  FOREIGN KEY (user_id) REFERENCES users (id)
    ON DELETE CASCADE
);
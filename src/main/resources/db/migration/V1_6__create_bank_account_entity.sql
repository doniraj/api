CREATE TABLE bank_accounts (
  id      INT PRIMARY KEY AUTO_INCREMENT,
  account VARCHAR(50) NOT NULL,
  user    INT         NOT NULL,
  FOREIGN KEY (user) REFERENCES users (id)
    ON DELETE CASCADE
);

CREATE UNIQUE INDEX bank_account_is_unique
  ON bank_accounts (account);


INSERT bank_accounts
  SELECT
    NULL         id,
    bank_account account,
    id           user
  FROM users u
  WHERE u.bank_account IS NOT NULL;


INSERT bank_accounts
  SELECT
    NULL                     id,
    alternative_bank_account account,
    id                       user
  FROM users u
  WHERE u.alternative_bank_account IS NOT NULL;

ALTER TABLE users
  DROP bank_account;

ALTER TABLE users
  DROP alternative_bank_account;

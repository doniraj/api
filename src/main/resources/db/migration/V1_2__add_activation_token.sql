CREATE TABLE verification_token (
  id          INT PRIMARY KEY AUTO_INCREMENT,
  token       VARCHAR(255),
  user_id     INT NOT NULL,
  expiry_date DATETIME,
  FOREIGN KEY (user_id) REFERENCES users (id)
    ON DELETE CASCADE
);

ALTER TABLE users
  DROP COLUMN confirmation_key;

ALTER TABLE users
  DROP COLUMN used_key;

ALTER TABLE users
  ADD COLUMN activated BOOLEAN;

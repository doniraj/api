CREATE TABLE users
(
  id                       INT PRIMARY KEY AUTO_INCREMENT,
  email                    VARCHAR(255) NOT NULL,
  user_password            VARCHAR(255) NOT NULL,
  confirmation_key         INTEGER,
  used_key                 INTEGER,
  acc_name                 VARCHAR(20)  NOT NULL,
  acc_surname              VARCHAR(20)  NOT NULL,
  age                      SMALLINT     NOT NULL,
  bank_account             VARCHAR(50),
  alternative_bank_account VARCHAR(50),
  role                     VARCHAR(20)
);

CREATE TABLE story
(
  id           INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  user_id      INT             NOT NULL,
  title        VARCHAR(255)    NOT NULL,
  date_created DATETIME,
  funds        BIGINT          NOT NULL,
  story        VARCHAR(2000)   NOT NULL, #todo: change to description
  FOREIGN KEY (user_id) REFERENCES users (id)
    ON DELETE CASCADE
);

CREATE UNIQUE INDEX users_email_uindex
  ON users (email);
CREATE UNIQUE INDEX users_confirmation_key_uindex
  ON users (confirmation_key);
CREATE UNIQUE INDEX users_used_key_uindex
  ON users (used_key);

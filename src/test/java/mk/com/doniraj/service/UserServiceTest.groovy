package mk.com.doniraj.service

import com.auth0.jwt.exceptions.TokenExpiredException
import mk.com.doniraj.exceptions.InvalidTokenException
import mk.com.doniraj.model.PasswordResetToken
import mk.com.doniraj.model.User
import mk.com.doniraj.model.VerificationToken
import mk.com.doniraj.repository.BankAccountRepository
import mk.com.doniraj.repository.PasswordResetTokenRepository
import mk.com.doniraj.repository.UserRepository
import mk.com.doniraj.repository.VerificationTokenRepository
import org.springframework.dao.DataIntegrityViolationException
import org.springframework.dao.DataRetrievalFailureException
import org.springframework.security.crypto.password.PasswordEncoder
import spock.lang.Specification

class UserServiceTest extends Specification {
    private UserRepository userRepository
    private VerificationTokenRepository verificationTokenRepository
    private PasswordResetTokenRepository resetTokenRepository
    private BankAccountRepository bankAccountRepository
    private PasswordEncoder passwordEncoder
    private UserService userService

    void setup() {
        userRepository = Mock(UserRepository.class)
        verificationTokenRepository = Mock()
        resetTokenRepository = Mock()
        passwordEncoder = Mock()
        bankAccountRepository = Mock()
        userService = new UserService(
                userRepository,
                verificationTokenRepository,
                resetTokenRepository,
                bankAccountRepository,
                passwordEncoder)

        //mock save method for user repository
        userRepository.save(_ as User) >> { User u -> u }
        //mock save method for password reset repository
        resetTokenRepository.save(_ as PasswordResetToken) >> { PasswordResetToken token -> token }
        //mock save method for verification repository
        verificationTokenRepository.save(_ as VerificationToken) >> { VerificationToken token -> token }
    }

    def "SaveUser"() {
        given:
        User user = new User("email",
                "password",
                "name",
                "surname",
                new Date()
                ,
                "role",
                "phone",
                "url")


        when:
        boolean bool = userService.saveUser(user)

        then:
        bool

    }

    def "AddUser"() {
        given:

        User user = new User("email",
                "password",
                "name",
                "surname",
                new Date()
                ,
                "role",
                "phone",
                "url")


        when:
        userService.addUser(user)

        then:
        notThrown(DataIntegrityViolationException)
    }

    def "GenerateVerificationToken"() {
        given:
        User user = new User()


        when:
        VerificationToken verificationToken = userService.generateVerificationToken(user)

        then:
        verificationToken.expiryDate.after(new Date())
    }

    def "ActivateUser"() {
        given:

        User user = new User()

        verificationTokenRepository.findByToken(_ as String) >> { String token ->
            VerificationToken verificationToken = new VerificationToken(token, user)
        }


        expect:
        userService.activateUser(token) != null

        where:
        token  | _
        "123"  | _
        "daf"  | _
        "j@i1" | _
    }

    def "failActivateUser"() {
        given:

        User user = new User()

        verificationTokenRepository.findByToken(_ as String) >> { String token ->
            VerificationToken verificationToken = new VerificationToken("123", null)
        }


        when:
        userService.activateUser("321")

        then:
        thrown(InvalidTokenException)
    }

    def "expiredActivateUser"() {
        given:

        VerificationToken token = new VerificationToken()
        token.setExpiryDate(new Date())

        verificationTokenRepository.findByToken(_ as String) >> token

        when:
        userService.activateUser("123")

        then:
        thrown(TokenExpiredException)
    }

    def "GetAllUsers"() {
        given:
        List<User> users = new ArrayList<>()

        userRepository.findAll() >> users

        when:
        userService.getAllUsers()

        then:
        notThrown(DataRetrievalFailureException)
    }

    def "DeleteUser"() {
    }

    def "GetUser"() {
    }

    def "RequestResetPassword"() {
        given:
        userRepository.findByEmail(_ as String) >> { String email ->
            User user = new User()
            user.email = email
            user
        }

        when:
        userService.requestResetPassword("email")

        then:
        notThrown(DataIntegrityViolationException)
    }

    def "ResetPassword"() {
        given:
        User user = new User()

        resetTokenRepository.findByToken(_ as String) >> {
            PasswordResetToken token = new PasswordResetToken()
            token.user = user
            token
        }

        when:
        userService.resetPassword(_ as String, _ as String)

        then:
        1 * passwordEncoder.encode(_ as String)
    }

    def "Update"() {
        given:
        userRepository.findByEmail(_ as String) >> { String email ->
            User user = new User()
            user.email = email
            user
        }

        String email = "email"
        User user = new User(email)
        when:
        userService.update(user, email)

        then:
        notThrown(DataRetrievalFailureException)
    }

}

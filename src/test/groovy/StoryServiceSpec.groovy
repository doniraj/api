import mk.com.doniraj.repository.StoryRepository
import mk.com.doniraj.repository.UserRepository
import mk.com.doniraj.service.StoryService
import org.springframework.boot.autoconfigure.security.oauth2.resource.UserInfoRestTemplateCustomizer
import org.springframework.dao.DataRetrievalFailureException
import spock.lang.Specification

class StoryServiceSpec extends Specification {

    private UserRepository mockUserRespository;
    private StoryRepository mockStoryRespository;

    private StoryService storyService;

    def setup() {
        mockUserRespository = Mock();
        mockStoryRespository = Mock();

        storyService = new StoryService(mockUserRespository, mockStoryRespository);
    }


    def "Should throw an DataRetrievalFailureException when non exsisting story id is passed"() {
        given:
        def storyId = -1

        when:
        storyService.getStoryById(storyId);

        then:
        DataRetrievalFailureException exception = thrown(DataRetrievalFailureException)
        exception != null
        exception.message != null
    }


}
